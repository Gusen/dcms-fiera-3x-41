<?
include_once '../sys/inc/start.php';
include_once '../sys/inc/compress.php';
include_once '../sys/inc/sess.php';
include_once '../sys/inc/home.php';
include_once '../sys/inc/settings.php';
include_once '../sys/inc/db_connect.php';
include_once '../sys/inc/ipua.php';
include_once '../sys/inc/fnc.php';
include_once '../sys/inc/adm_check.php';
include_once '../sys/inc/user.php';
user_access('modules_edit',null,'index.php?'.SID);
adm_check();
$set['title'] = lang('Управление админ разделами');
include_once H.'sys/inc/thead.php';
title();
  
lang::start('default');

if (isset($_POST['add']) && isset($_POST['name']) && $_POST['name']!=NULL && isset($_POST['url']) && $_POST['url']!=NULL && isset($_POST['counter']))
{
$name= my_esc($_POST['name']);
$url= my_esc($_POST['url']);
$counter = my_esc($_POST['counter']);

$pos = mysql_result(mysql_query("SELECT MAX(`pos`) FROM `admin_menu`"), 0)+1;

$accesses = my_esc($_POST['accesses']);



$icon=preg_replace('#[^a-z0-9 _\-\.]#i', null, $_POST['icon']);
query("INSERT INTO `admin_menu` (`name`, `url`, `counter`, `pos`, `img`,`time`,`accesses`) VALUES ('$name', '$url', '$counter', '$pos', '$icon','".time()."','$accesses')");

$_SESSION['message'] = lang('Ссылка успешно добавлена');
exit(header('Location: ?'));
 
}


if (isset($_POST['change']) && isset($_GET['id']) && isset($_POST['name']) && $_POST['name']!=NULL)
{
$id=intval($_GET['id']);

$name= my_esc($_POST['name']);
$url= my_esc($_POST['url']);
$counter = my_esc($_POST['counter']);


$accesses = my_esc($_POST['accesses']);



if (isset($_POST['icon']))
$icon = preg_replace('#[^a-z0-9 _\-\.]#i', null, $_POST['icon']);
else
$icon = 'default.png';

query("UPDATE `admin_menu` SET `name` = '$name', `url` = '$url', `counter` = '$counter', `img` = '$icon' ,`accesses` = '$accesses' WHERE `id` = '$id' LIMIT 1");
$_SESSION['message'] = lang('Пункт меню успешно изменен');
exit(header('Location: ?'));
 
}

if (isset($_GET['id']) && isset($_GET['act']) && mysql_result(mysql_query("SELECT COUNT(*) FROM `admin_menu` WHERE `id` = '".intval($_GET['id'])."'"),0))
{

$menu=mysql_fetch_assoc(mysql_query("SELECT * FROM `admin_menu` WHERE `id` = '".intval($_GET['id'])."' LIMIT 1"));
if ($_GET['act']=='up')
{
mysql_query("UPDATE `admin_menu` SET `pos` = '".($menu['pos'])."' WHERE `pos` = '".($menu['pos']-1)."' LIMIT 1");
mysql_query("UPDATE `admin_menu` SET `pos` = '".($menu['pos']-1)."' WHERE `id` = '".intval($_GET['id'])."' LIMIT 1");

$_SESSION['message'] = lang('Пункт меню сдвинут на позицию вверх');
exit(header('Location: ?'));
 
}
if ($_GET['act']=='down')
{
mysql_query("UPDATE `admin_menu` SET `pos` = '".($menu['pos'])."' WHERE `pos` = '".($menu['pos']+1)."' LIMIT 1");
mysql_query("UPDATE `admin_menu` SET `pos` = '".($menu['pos']+1)."' WHERE `id` = '".intval($_GET['id'])."' LIMIT 1");

$_SESSION['message'] = lang('Пункт меню сдвинут на позицию вниз');
exit(header('Location: ?'));
}
if ($_GET['act']=='del')
{

mysql_query("DELETE FROM `admin_menu` WHERE `id` = '".intval($_GET['id'])."' LIMIT 1");


$_SESSION['message'] = lang('Пункт меню удален');
exit(header('Location: ?'));
}


}
 
  

err();
aut();
echo "<table class='post'>";

$q=mysql_query("SELECT * FROM `admin_menu` ORDER BY `pos` ASC");
while ($post = mysql_fetch_assoc($q))
{
echo "   <tr>\n";
echo "  <td class='adm_panel'>";

//Выводим иконку
if ($post['type'] == 'link') 
echo image::is("/style/icons_admpanel/". $post['img']);

else echo "<img src='/style/icons_admpanel/razd.png'> ";

echo  lang($post['name']) ." <span style='float:right' class='adm_panel_span'>  <a href='?id=$post[id]&amp;act=edit'>".lang('Редактировать')." </a></span>";
echo "  </td>\n";
echo "   </tr>\n";
echo "   <tr>\n";
echo "  <td class='p_m'>\n";

echo ($post['type'] == 'link' ?   lang('Ссылка') . ': '.$post['url'].' <br/>': null);
 

 
if (isset($_GET['id']) && $_GET['id']==$post['id'] && isset($_GET['act']) && $_GET['act']=='edit')
{

echo "<form action=\"?id=$post[id]\" method=\"post\">";
echo lang('Тип') .': '.($post['type'] == 'link' ? lang('Ссылка') : lang('Разделитель'))."<br />";

echo lang('Название')." :<br />";
echo "<input type='text' name='name' value='".lang($post['name'])."' /><br />";

echo lang('Права доступа').":<br />";
echo "<input type=\"text\" name=\"accesses\" value=\"".($post['accesses'])."\"/><br />";

if ($post['type']=='link')
{
echo lang('Ссылка').":<br />";
echo "<input type='text' name='url' value='$post[url]' /><br />";
}
else
echo "<input type='hidden' name='url' value='' />";

echo lang('Счетчик').":<br />";
echo "<input type='text' name='counter' value='$post[counter]' /><br />\n";
if ($post['type']=='link')
{

echo lang('Иконка').": <br/><input type='text' name='icon' value='$post[img]' /><br />";

} 


echo "<input class=\"submit\" name=\"change\" type=\"submit\" value='".lang('Изменить')."' /><br />\n";
echo "</form>";


echo "<a href='?'>".lang('Отмена')."</a><br />";
}
else
{
 
echo lang('Счетчик').': '.($post['counter'] == null ? lang('отсутствует') : $post['counter'])."<br /><br />";
echo "<span class='adm_panel_span'><a href='?id=$post[id]&amp;act=up&amp;$passgen'>".lang('Выше')."</a> | ";
echo "<a href='?id=$post[id]&amp;act=down&amp;$passgen'>".lang('Ниже')."</a> | ";
echo "<a href='?id=$post[id]&amp;act=del&amp;$passgen'> ".lang('Удалить')."</a></span><br /><br />";

}

echo "  </td>";
echo "   </tr>";
}


echo "</table>";


if (isset($_GET['add']))
{
echo "  <div class='p_m'>";
echo "<form action='?add=$passgen' method=\"post\">";
echo "Тип:<br />";
echo "<select name='type'>";
echo "<option value='link'> ".lang('Ссылка')."</option>";
echo "<option value='razd'> ".lang('Раздел')."</option>";
echo "</select><br />";
echo lang('Название').":<br />";
echo "<input type=\"text\" name=\"name\" value=\"\"/><br />";
echo lang('Права доступа').":<br />";
echo "<input type=\"text\" name=\"accesses\" value=\"\"/><br />";
echo lang('Ссылка').":<br />\n";
echo "<input type=\"text\" name=\"url\" value=\"\"/><br />";
echo lang('Счетчик')." (".APANEL."/count/*):<br />\n";
echo "<input type=\"text\" name=\"counter\" value=\"\"/><br />";
echo lang('Иконка')." :<br />\n";
echo "<input type='text' name='icon' value='default.png' /><br />";
echo "<input class='submit' name='add' type='submit' value='".lang('Добавить')."' /><br />";
echo "<a href='?$passgen'>".lang('Отмена')."</a><br />\n";
echo "</form></div>";
}
else echo "<div class='foot'><a href='?add=$passgen'>".lang('Добавить пункт')."</a></div>";

echo "<div class='foot'>";
echo "&laquo;<a href='".APANEL."/'>".lang('В админку')."</a><br />";
echo "</div>";


include_once '../sys/inc/tfoot.php';
?>